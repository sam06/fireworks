package com.sam.fireworks.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: WareApp
 * @description: TODO
 * @Author: sam
 * @date: 2021/7/25 17:18
 */
@SpringBootApplication
public class WareApp {
    public static void main(String[] args) {
        SpringApplication.run(WareApp.class,args);
    }
}
