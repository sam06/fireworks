package com.sam.firworks.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: MemberApp
 * @description: TODO
 * @Author: sam
 * @date: 2021/7/25 17:15
 */
@SpringBootApplication
public class MemberApp {
    public static void main(String[] args) {
        SpringApplication.run(MemberApp.class,args);
    }
}
