package com.sam.firworks.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: CouponApp
 * @description: TODO
 * @Author: sam
 * @date: 2021/7/25 17:14
 */
@SpringBootApplication
public class CouponApp {
    public static void main(String[] args) {
        SpringApplication.run(CouponApp.class,args);
    }
}
