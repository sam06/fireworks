package com.sam.firworks.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname: ProductApp
 * @description: TODO
 * @Author: sam
 * @date: 2021/7/25 17:17
 */
@SpringBootApplication
public class ProductApp {
    public static void main(String[] args) {
        SpringApplication.run(ProductApp.class,args);
    }
}
